import React, {useState} from "react";
import Burger from "./components/Burger/Burger";
import meatImage from './assets/meat.jpg';
import cheeseImage from './assets/cheese.jpg';
import saladImage from './assets/salad.png';
import baconImage from './assets/bacon.jpg';
import './App.css';
import Ingredients from "./components/Ingredients/Ingredients";

const INGREDIENTS = [
    {name: 'Meat', price: 50, image: meatImage, id: 0},
    {name: 'Cheese', price: 20, image: cheeseImage, id: 1},
    {name: 'Salad', price: 5, image: saladImage, id: 2},
    {name: 'Bacon', price: 30, image: baconImage, id: 3}
];

const App = () => {
    const [ingredients, setIngredients] = useState([
        {name: 'Meat', count: 0},
        {name: 'Cheese', count: 0},
        {name: 'Salad', count: 0},
        {name: 'Bacon', count: 0},
    ]);

    const addCount = (id, name) => {
        if (ingredients[id].name === name) {
            return ingredients[id].count
        }
    };

    const addIngredient = (e, name) => {
        setIngredients(ingredients.map((ingredient) => {
            if (name === ingredient.name) {
                return {
                    ...ingredient,
                    count: ingredient.count + 1
                };
            }

            return ingredient;
        }));
    };

    const removeIngredient = (e, name) => {
        setIngredients(ingredients.map((ingredient) => {
            if (name === ingredient.name) {
                if (ingredient.count > 0) {
                    return {
                        ...ingredient,
                        count: ingredient.count - 1
                    };
                }
            }

            return ingredient;
        }));
    };

    const getTotalPrice = () => {
        const total = [20];

        for (let i = 0; i < INGREDIENTS.length; i++) {
            if (INGREDIENTS[i].name === ingredients[i].name) {
                total.push(INGREDIENTS[i].price * ingredients[i].count);
            }
        }

        return total.reduce((acc, number) => {
            acc += number;
            return acc;
        }, 0);
    };

    return (
        <div className="container">
            <div className="constructor-burger">
                <div className="ingredients-block">
                    <fieldset>
                        <legend>Ingredients</legend>
                        <Ingredients
                            ingredients={INGREDIENTS}
                            addIng={addIngredient}
                            count={addCount}
                            removeIng={removeIngredient}
                        />
                    </fieldset>
                </div>
                <div className="burger-block">
                    <fieldset>
                        <legend>Burger</legend>
                        <Burger
                            meatCount={ingredients[0].count}
                            cheeseCount={ingredients[1].count}
                            saladCount={ingredients[2].count}
                            baconCount={ingredients[3].count}
                        />
                        <p style={{textAlign: 'center'}}>Price: <span>{getTotalPrice()}</span></p>
                    </fieldset>
                </div>
            </div>
        </div>
    );
};

export default App;
