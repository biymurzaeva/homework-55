import React from 'react';
import Ingredient from "../Ingredient/Ingredient";
import './Ingredients.css';

const Ingredients = props => {
    const ingredients = props.ingredients.map((ing) => (
        <Ingredient
            key={ing.id}
            name={ing.name}
            ingImg={ing.image}
            count={props.count(ing.id, ing.name)}
            addIngredient={(e) => {props.addIng(e.target, ing.name)}}
            removeIngredient={(e) => {props.removeIng(e.target, ing.name)}}
        />
    ));

    return (
        <div className="ingredients">
            {ingredients}
        </div>
    );
};

export default Ingredients;