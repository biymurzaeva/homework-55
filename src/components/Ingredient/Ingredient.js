import React from 'react';
import './Ingredient.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/free-solid-svg-icons";

const Ingredient = props => {
    return (
        <div className="ingredient">
            <div className="ing-img-block" onClick={props.addIngredient}>
                <img src={props.ingImg} alt={props.name} className="ing-img"/>
            </div>
            <p>{props.name}</p>
            <p>x{props.count}</p>
            <FontAwesomeIcon icon={faTrashAlt}  size="2x" onClick={props.removeIngredient}/>
        </div>
    );
};

export default Ingredient;