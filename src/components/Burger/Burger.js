import React from 'react';
import Meat from "../Meat/Meat";
import Cheese from "../Cheese/Cheese";
import Salad from "../Salad/Salad";
import Bacon from "../Bacon/Bacon";
import './Burger.css';

const Burger = props => {
    const meat = [];
    const cheese = [];
    const salad = [];
    const bacon = [];

    for (let i = 0; i < props.meatCount; i++) {
        meat.push(<Meat key={i}/>);
    }

    for (let i = 0; i < props.cheeseCount; i++) {
        cheese.push(<Cheese key={i}/>);
    }

    for (let i = 0; i < props.saladCount; i++) {
        salad.push(<Salad key={i}/>);
    }

    for (let i = 0; i < props.baconCount; i++) {
        bacon.push(<Bacon key={i}/>);
    }

    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"/>
                <div className="Seeds2"/>
            </div>
            {salad}
            {bacon}
            {cheese}
            {meat}
            <div className="BreadBottom"/>
        </div>
    );
};

export default Burger;